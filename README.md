
# Pocket Distro

A trivial example of using BuildRoot to create a super simple embedded linux image of x86.

## Build

```
$ make BR2_EXTERNAL=path/to/PocketDistro pocketdistro_defconfig
```

rebuild:
```
$ make pocketinit-rebuild


$ make -C build_initrd pocketinit-rebuild && make -C build_rootfs && make run

```



## Run

```
$ qemu-system-i386 -kernel output/images/bzImage -initrd output/images/rootfs.cpio.gz
```

http://imrannazar.com/Booting-Linux-from-Flash

https://landley.net/writing/rootfs-programming.html