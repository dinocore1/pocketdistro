#!/bin/bash

pushd build_initrd
make pocketinit-rebuild
make
popd

pushd build_rootfs
make
popd

make run