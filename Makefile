
#BUILDROOT_URL ?= git://git.buildroot.net/buildroot
#BUILDROOT_GIT_SHA ?= 2019.11.1
BUILDROOT_URL ?= https://github.com/dinocore1/buildroot.git
BUILDROOT_GIT_SHA ?= syslinux_gptmbr
BUILDROOT_DIR := buildroot
BUILDROOT_INITRD := build_initrd
BUILDROOT_ROOTFS := build_rootfs

.PHONY: all run clean run_init_shell

all:


$(BUILDROOT_DIR)/.git:
	git clone $(BUILDROOT_URL) $(BUILDROOT_DIR) && \
	cd $(BUILDROOT_DIR) && \
	git checkout $(BUILDROOT_GIT_SHA)

### INITRD Buildroot

$(BUILDROOT_INITRD)/Makefile: $(BUILDROOT_DIR)/.git
	rsync -a $(BUILDROOT_DIR)/ $(BUILDROOT_INITRD)

$(BUILDROOT_INITRD)/.config: $(BUILDROOT_INITRD)/Makefile
	make -C $(abspath $(BUILDROOT_INITRD)) BR2_EXTERNAL=$(abspath pocketdistro) pocketdistro_initrd_defconfig

$(BUILDROOT_INITRD)/output/images/rootfs.cpio.gz: $(BUILDROOT_INITRD)/.config
	make -C $(abspath $(BUILDROOT_INITRD))

### rootfs Buildroot

$(BUILDROOT_ROOTFS)/Makefile: $(BUILDROOT_DIR)/.git
	rsync -a $(BUILDROOT_DIR)/ $(BUILDROOT_ROOTFS)

$(BUILDROOT_ROOTFS)/.config: $(BUILDROOT_ROOTFS)/Makefile
	make -C $(abspath $(BUILDROOT_ROOTFS)) BR2_EXTERNAL=$(abspath pocketdistro) pocketdistro_defconfig

$(BUILDROOT_ROOTFS)/output/images/usbthumb.img: $(BUILDROOT_INITRD)/output/images/rootfs.cpio.gz $(BUILDROOT_ROOTFS)/.config
	make -C $(abspath $(BUILDROOT_ROOTFS))

clean:
	rm -rf $(BUILDROOT_INITRD)
	rm -rf $(BUILDROOT_ROOTFS)

run: $(BUILDROOT_ROOTFS)/output/images/usbthumb.img
	qemu-system-i386 -hda $(BUILDROOT_ROOTFS)/output/images/usbthumb.img

run_init_shell: $(BUILDROOT_ROOTFS)/output/images/usbthumb.img
	qemu-system-i386 -kernel $(BUILDROOT_ROOTFS)/output/images/bzImage -initrd $(BUILDROOT_INITRD)/output/images/rootfs.cpio.gz  -hda $(BUILDROOT_ROOTFS)/output/images/usbthumb.img -append rdinit=/bin/sh