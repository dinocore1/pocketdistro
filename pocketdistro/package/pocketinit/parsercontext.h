#pragma once

#include <string>
#include <iostream>

namespace pocketinit
{

class BaseParserContext
{
public:
  BaseParserContext();
  virtual ~BaseParserContext();

  int readBytes(char* buf, const int max);
  bool parse(const std::string& streamName, std::istream& is);
  void parseError(const int line, const int column, const char* errMsg);

protected:
  void* mScanner;
  std::istream* mInputStream;
  std::string mStreamName;
};

} // namespace pocketinit