#pragma once

#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include "udev.tab.hh"
#include "location.hh"

namespace pocketinit
{

class UDev_Scanner : public yyFlexLexer
{
public:
  UDev_Scanner(std::istream* in) : yyFlexLexer(in)
  {
    loc = new pocketinit::UDev_Parser::location_type();
  }

  using FlexLexer::yylex;

  /** YY_DECL defined int udev.l
   * method body created by flex in udev.yy.cc
   */
  virtual
  int yylex(pocketinit::UDev_Parser::semantic_type* const lval,
            pocketinit::UDev_Parser::location_type* location);

private:
  pocketinit::UDev_Parser::semantic_type* yylval = nullptr;
  pocketinit::UDev_Parser::location_type* loc = nullptr;
};

} // pocketinit