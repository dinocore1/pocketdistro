
#include "parsercontext.h"

namespace pocketinit
{

BaseParserContext::BaseParserContext()
{
}

BaseParserContext::~BaseParserContext()
{
}

int BaseParserContext::readBytes(char* buf, const int max)
{
  mInputStream->read(buf, max);
  int bytesRead = mInputStream->gcount();
  return bytesRead;
}

} // namespace pocketinit