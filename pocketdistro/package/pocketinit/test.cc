#include <gtest/gtest.h>

#include <string>

#include "udev_file.h"

using namespace pocketinit;

extern "C" {
#include <gc.h>
}


TEST(udev_parser, test1)
{

  const std::string raw_input(R"(
    
    INPUT=kewl
    
  )");

  std::istringstream istream(raw_input);

  UDevFile driver;
  driver.parse(istream);


}