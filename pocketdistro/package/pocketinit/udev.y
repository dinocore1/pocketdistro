%skeleton "lalr1.cc"
%require "3.2"
%language "c++"
%debug
%defines
%define api.namespace {pocketinit}
%define api.parser.class {UDev_Parser}

%parse-param { UDev_Scanner &scanner }
%parse-param { UDevFile &driver }

%code requires{

  namespace pocketinit {
    class UDevFile;
    class UDev_Scanner;
  }
}

%code{
  #include <iostream>
  #include <cstdlib>
  #include <fstream>

  #include "udev_file.h"
  
  #undef yylex
  #define yylex scanner.yylex
}

%define api.value.type variant
%define parse.assert
%locations

%token TEQUALS
%token <std::string> TID
%token TNEWLINE
%token TEND 0 "end of file"


%%

udev_file
  : TEND
  | list TEND
  ;
  
list
  : keyvalue
  | list keyvalue
  ;

keyvalue
  : TID TEQUALS TID { driver.add_keyvalue($1, $3); }
  | TNEWLINE
  ;

%%

void
pocketinit::UDev_Parser::error( const location_type &l, const std::string &err_message )
{
  std::cerr << "Error: " << err_message << " at " << l << std::endl;
}