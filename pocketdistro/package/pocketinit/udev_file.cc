
#include "udev_file.h"

namespace pocketinit
{

UDevFile::~UDevFile()
{

}

void UDevFile::parse(std::istream& iss)
{

  scanner = new UDev_Scanner(&iss);
  parser = new UDev_Parser(*scanner, *this);
  //parser->set_debug_stream( std::cout );
  //parser->set_debug_level( 5 );
  const int accept(0);
  if(parser->parse() != accept) {
    std::cerr << "Parse failed" << std::endl;
  }
}

void UDevFile::add_keyvalue(const std::string& key, const std::string& value)
{
  std::cout << "add key-value " << key << " : " << value << std::endl;
}

}