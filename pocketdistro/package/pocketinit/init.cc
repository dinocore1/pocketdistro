#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>

#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "pocketinit.h"

enum {
  L_LOG = 0x1,
  L_CONSOLE = 0x2,
};

static void message(int where, const char* fmt, ...)
__attribute__((format(printf, 2, 3)));


/*
 * Write all of the supplied buffer out to a file.
 * This does multiple writes as necessary.
 * Returns the amount written, or -1 on an error.
 */
static ssize_t full_write(int fd, const void* buf, size_t len)
{
  ssize_t cc;
  ssize_t total;

  total = 0;

  while(len) {
    cc = write(fd, buf, len);

    if(cc < 0) {
      if(total) {
        /* we already wrote some! */
        /* user can do another write to know the error code */
        return total;
      }
      return cc;  /* write() returns -1 on failure. */
    }

    total += cc;
    buf = ((const char*)buf) + cc;
    len -= cc;
  }

  return total;
}

static void message(int where, const char* fmt, ...)
{
  va_list arguments;
  unsigned l;
  char msg[128];

  msg[0] = '\r';
  va_start(arguments, fmt);
  l = 1 + vsnprintf(msg + 1, sizeof(msg) - 2, fmt, arguments);
  if(l > sizeof(msg) - 2) {
    l = sizeof(msg) - 2;
  }
  va_end(arguments);

  msg[l] = '\0';
  msg[l++] = '\n';
  msg[l] = '\0';

  if(where & L_CONSOLE) {
    full_write(STDERR_FILENO, msg, l);
  }
}

static void console_init()
{
  int vtno;
  char* s;

  s = getenv("CONSOLE");
  if(!s) {
    s = getenv("console");
  }

  if(s) {
    int fd = open(s, O_RDWR | O_NONBLOCK | O_NOCTTY);
    if(fd >= 0) {
      dup2(fd, STDIN_FILENO);
      dup2(fd, STDOUT_FILENO);
      dup2(fd, STDERR_FILENO);
      close(fd);
    }
    message(L_CONSOLE, "console='%s'", s);
  }


}

static int run(const char* exe_path, char* const* cmd)
{
  int pid;
  int err;

  pid = fork();
  if(pid == 0) {
    //child
    setsid();

    /* Open the new terminal device */
    //if (!open_stdio_to_tty(a->terminal))
    //    exit(EXIT_FAILURE);

    err = execvp(exe_path, cmd);
    message(L_LOG | L_CONSOLE, "can't run '%s': %s", exe_path, strerror(errno));
  } else {
    return pid;
  }

  return pid;
}

int main(int argc, char* argv[])
{

  if(getpid() != 1) {
    message(L_CONSOLE, "must be run as PID 1");
    exit(-1);
  }

  console_init();

  chdir("/");
  setsid();

  /* Make sure environs is set to something sane */
  putenv((char*) "HOME=/");
  putenv((char*) "PATH=/sbin:/usr/sbin:/bin:/usr/bin");
  putenv((char*) "SHELL=/bin/sh");
  putenv((char*) "USER=root");

	message(L_CONSOLE | L_LOG, "pocketinit started");

  const char* const args[] = {
    "sh",
    NULL
  };

  //run("/bin/sh", args);
	execvp("/bin/sh", const_cast<char* const*>(args));

  while(1) {
    //puts("hello");


	/* Wait for any child process(es) to exit */
		while (1) {
			pid_t wpid;
			struct init_action *a;

			wpid = waitpid(-1, NULL, WNOHANG);
			if (wpid <= 0)
				break;

			message(L_CONSOLE, "pid %u exited", (unsigned) wpid);

		}

    sleep(1);
  }
}

void yyerror(const char *s) {
  printf("EEK, parse error %s\n", s);
  exit(-1);
}