%{

  #include "dsl.tab.h"

%}

%%

[ \t\n]                     ;
"{"                         { return TLBRACE; }
"}"                         { return TRBRACE; }
"service"                   { return TSERVICE; }
"on"                        { return TON; }
[a-zA-Z0-9_]*               { yylval.string = strdup(yytext); return TID; }

%%