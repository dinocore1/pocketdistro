#pragma once

#include <string>
#include <cstddef>
#include <istream>

#include "udev_scanner.h"
#include "udev.tab.hh"

namespace pocketinit
{

class UDevFile
{
public:
  UDevFile() = default;
  virtual ~UDevFile();

  void parse(std::istream& iss);

  void add_keyvalue(const std::string& key, const std::string& value);

private:

  UDev_Parser* parser;
  UDev_Scanner* scanner;


};

} // namespace pocketinit