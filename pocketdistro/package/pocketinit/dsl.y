
%{
  #include <stdio.h>

  // stuff from flex that bison needs to know about:
  extern int yylex();
  extern int yyparse();
  extern FILE *yyin;
 
  void yyerror(const char *s);

%}

%union {
  char* string;
  void* list;
}

%token TSERVICE TON TLBRACE TRBRACE
%token <string> TID
%type <list> arglist

%%

service
  : TSERVICE TID cmd TLBRACE TRBRACE
  ;

event
  : TON TID TLBRACE TRBRACE
  ;

cmd
  : TID arglist
  ;

arglist
  : arglist TID
  | {  }
  ;

%%