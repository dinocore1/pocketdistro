#!/bin/sh
BOARD_DIR="$(dirname $0)"

install -m 0644 -D $BOARD_DIR/syslinux.cfg $BINARIES_DIR/syslinux.cfg

install -m 0644 -D $BOARD_DIR/../../build_initrd/output/images/rootfs.cpio.gz $BINARIES_DIR/initrd.gz
support/scripts/genimage.sh -c $BOARD_DIR/genimage_boot.cfg

echo "running syslinux onb boot.vfat"
syslinux -i ${BINARIES_DIR}/boot.vfat

support/scripts/genimage.sh -c $BOARD_DIR/genimage_final.cfg

## Special thanks to https://lightofdawn.org/wiki/wiki.cgi/-wiki/BIOSBootGPT
sgdisk -A 1:set:2 ${BINARIES_DIR}/usbthumb.img
dd if=${BINARIES_DIR}/syslinux/gptmbr.bin of=${BINARIES_DIR}/usbthumb.img bs=440 count=1 conv=notrunc

